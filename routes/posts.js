module.exports = app => {
    const posts = require("../controllers/posts");

    const router = require("express").Router();

    // Retrieve all Tutorials
    router.get("/", posts.findAll);

    app.use("/", router);
};