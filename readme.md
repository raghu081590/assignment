1) Run npm install in root folder
2) Postgres: (Database used is postgres(sql database) as the data is structured data and is good for maintacommining relations).
    Run the following commands in postgres shell to create database and user.
    a) create database assignment.
    b) create user assignment with encrypted password 'p@ssw0rd';
    c) grant all privileges on database assignment to assignment;
3) Run "npm start" to run the server which will also create Tables : Posts, Users in Database if not already present.
4) Run "node data.js" to import data from "https://jsonplaceholder.typicode.com/".
5) Open "http://localhost:3000/" in browser to view posts in a table.
