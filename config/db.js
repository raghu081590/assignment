module.exports = {
    HOST: "localhost",
    USER: "assignment",
    PASSWORD: "p@ssw0rd",
    DB: "assignment",
    dialect: "postgres",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};