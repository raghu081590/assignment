const db = require("../models");
const Post = db.posts;
const User = db.users;


exports.findAll = (req, res) => {
    Post.findAll({include: User})
        .then(data => {
            res.render('index', {title: 'Posts', posts: data})
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving posts."
            });
        });
};