const express = require('express');
const app = express()

const db = require("./db");

// Sync and create tables if not already present in database.
db.sync();

// Views
app.set('view engine', 'pug')

// Routes
require("./routes/posts")(app);

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});