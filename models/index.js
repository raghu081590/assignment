const sequelize = require("../db");
const Sequelize = require("sequelize");

const db = {};
db.posts = require('./post')(sequelize, Sequelize);
db.users = require('./user')(sequelize, Sequelize);
db.posts.belongsTo(db.users);


module.exports = db;