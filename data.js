const TypicodeApi = require('./apis/typecode')

const db = require('./models/index')

TypicodeApi.getUsers().then(users => {
    users.forEach(function (user) {
        db.users.upsert(user).then(data =>
            console.log("Successfully added user with name : " + data[0].dataValues.name)
        ).catch(err => console.log(err))
    });
});


TypicodeApi.getPosts().then(posts => {
    posts.forEach(function (post) {
        db.posts.upsert(post).then(data =>
            console.log("Successfully added post with title  : " + data[0].dataValues.title)
        ).catch(err => console.log(err))
    });
});