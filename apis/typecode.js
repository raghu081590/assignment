const fetch = require('node-fetch')


class TypicodeApi {
    constructor() {
        this.base_url = "https://jsonplaceholder.typicode.com";
    }

    async getUsers() {
        const response = await fetch(this.base_url + '/users');
        return await response.json()
    };

    async getPosts() {
        const response = await fetch(this.base_url + '/posts');
        return await response.json()
    };
}


module.exports = new TypicodeApi()


